# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

1. Entrar a la carpeta test-bice-nodejs `cd test-bice-nodejs`.
2. Instalar modulos y dependencias `npm i`
3. Levantar el servicio en localhost `npm start`.
4. Paralelamente, mientras el servicio corre se puede ejecutar el test unitario en una nueva terminal. Entrando nuevamente a la carpeta del proyecto y ejecutando `npm test`.
