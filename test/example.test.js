const assert = require('assert');
const fetch = require('node-fetch');

describe('Timeout Test', () => {
    it('Servicio `convert` debe responder antes de 1seg', function (done) {

        this.timeout(1000);
        const config = {
            method: 'get'
        };    
        fetch('http://localhost:3000/indicators/convert/1/cobre/dolar', config)
        .then(res => {
            data = res.json();
            console.log(data);
            done();
        })
        .catch(err => console.error(err));
        
    });

    it('Servicio `last` debe responder antes de 0.5seg', function (done) {

        this.timeout(500);
        const config = {
            method: 'get'
        };    
        fetch('http://localhost:3000/indicators', config)
        .then(res => {
            data = res.json();
            console.log(data);
            done();
        })
        .catch(err => console.error(err));
        
    });
});