const fetch = require('node-fetch');
const baseUrl = 'https://www.indecon.online/';

const last = () => callendpoint(
        baseUrl + 'last',
        {},
        {},
        'get'
    );

const getByKey = (key) => callendpoint(
        baseUrl + 'values/' + key,
        {},
        {},
        'get'
    );

const callendpoint = async (url) =>{
    const config = {
        method: 'get'
    };    
    const response = await fetch(url, config);
    const data = await response.json();
    return { data };
};

exports.last = last;
exports.getByKey = getByKey;