const express = require('express');
const router = express.Router();
const indicatorsService = require('../services/indicators');

router.get('/', (req, res, next) => {
    indicatorsService.last().then(response => {
        res.status(200).json(response.data);
    }).catch( (err) => {
        console.error(err);
        res.status(500).send(err);
    });
});

router.get('/:key', (req, res, next) => {
    const key = req.params.key;
    indicatorsService.getByKey(key).then(response => {
        res.status(200).json(response.data);
    }).catch( (err) => {
        console.error(err);
        res.status(500).send(err);
    });
});

router.get('/convert/:qty/:keyInput/:keyOutput', (req, res, next) => {
    const qty = req.params.qty;
    const keyInput = req.params.keyInput;
    const keyOutput = req.params.keyOutput;
    indicatorsService.last().then(response => {
        let indicators = response.data;
        let valuePesosDolar = 0;
        let valuePesosInput = 0;
        let valuePesosOutput = 0;
        let subTotal = 0;
        let total = 0;

        for(let item in indicators){
            if(item === 'dolar'){
                valuePesosDolar = indicators[item].value;
            }
        }

        valuePesosInput = indicators[keyInput].unit === 'dolar' ? indicators[keyInput].value * valuePesosDolar : indicators[keyInput].value;
        valuePesosOutput = indicators[keyOutput].unit === 'dolar' ? indicators[keyOutput].value * valuePesosDolar : indicators[keyOutput].value;

        subTotal = qty * valuePesosInput;
        total = parseFloat((subTotal / valuePesosOutput).toFixed(3));

        res.status(200).json({ 'total': total });

    }).catch( (err) => {
        console.error(err);
        res.status(500).send(err);
    });
});


module.exports = router;